# Docker-compose for YII2-app-basic

## YII2 on Nginx (nginx version: nginx/1.15.8) + php-fpm (PHP 7.2.14) with MySQL (mysql  Ver 8.0.14), Redis (redis_version:5.0.3), RabbitMQ (rabbit,"RabbitMQ","3.7.10")
### Настраиваем подключение требуемых сервисов

    тут будет описание

### Настройка YII2
#### После клонирования конфигурации docker окружения, необходимо:

1. Установить `composer`, если не установлен
1. Установить проект `yii2-app-basic` ([installation official page](https://www.yiiframework.com/doc/guide/2.0/en/start-installation)): 
    
    ```composer create-project --prefer-dist yiisoft/yii2-app-basic app```
    
    <small><u><i>очень важно, чтоб установка проводилась в `app` директорию</i></u></small>
    
1. В файл `app/conf/db.php` необходимо установить следующие параметры:
```
    'dsn' => 'mysql:host=mysql;dbname=yii2basic',
    'username' => 'user_yii2basic',
    'password' => 'pass_yii2basic',
```

#### Структура проекта в результате:

    ./app/*             - Yii2 проект 
    ./docker/*          - файлы конфигурация окружения
    .gitignore
    docker-compose.yml  - файл конфигурации запуска docker-compose
    README.MD

### Запуск окружения docker-compose

    docker compose up --build
    
### Доступ к app

    http://localhost ( или http://localhost:80)
    
<small><u><i>адрес может быть любой, в зависимости от настроек docker-machine</i></u></small>

### Доступ к rabbitmq panel

    http://localhost:15672

<small><u><i>адрес может быть любой, в зависимости от настроек docker-machine и конфигурации docker-compose.yml</i></u></small>